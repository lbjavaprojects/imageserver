import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

public class ServerMain {

	private static HashMap<Integer,String> obrazkiMap;

	public static void main(String[] args) {
		initializeImageList();
		try(ServerSocket ss=new ServerSocket(8189)){
			while(true){
				Socket s=ss.accept();
				Thread t=new Thread(new ImageHandler(s));
				t.start();
			}
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	protected static void initializeImageList(){
		obrazkiMap=new HashMap<Integer, String>();
		obrazkiMap.put(1,"7649419_beskid-sadecki-piwniczna-zdroj--rzeka-poprad.jpg");
		obrazkiMap.put(2, "beskidsadecki2d.jpg");
		obrazkiMap.put(3, "panorama.png");
		obrazkiMap.put(4, "Radziejowa-18-660x330.jpg");
	}

	protected static BufferedImage getImage(int key){
		try{
			return ImageIO.read(new File(obrazkiMap.get(key)));
		}
		catch(IOException e){
			return null;
		}
	}
}
