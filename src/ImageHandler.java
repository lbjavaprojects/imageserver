import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;
import java.util.StringTokenizer;

public class ImageHandler implements Runnable {
	private Socket s;
	public ImageHandler(Socket s) {
		super();
		this.s=s;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try{
			try(InputStream is=s.getInputStream();
					Scanner sc=new Scanner(is);
					OutputStream os=s.getOutputStream();
					ObjectOutputStream oos=new ObjectOutputStream(os)
					){
				while(sc.hasNextLine()){
					String line=sc.nextLine();
					StringTokenizer st=new StringTokenizer(line);
					switch(st.nextToken()){
					case "get":{
						try{
							int key=Integer.valueOf(st.nextToken());
							BufferedImage img=ServerMain.getImage(key);
							if(img!=null){
                              oos.writeObject(img);
							}
						}
						catch(Exception e){
						}
					}
					case "end":{

					}
					case "quit":{

					}
					default:{

					}
					}
				}
			}
			catch(IOException e){

			}
		}
		finally{
			try{
				s.close();}
			catch(IOException e){
				e.printStackTrace();
			}
		}
	}

}
